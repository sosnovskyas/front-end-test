import React, {useEffect, useState} from 'react';

/*
FRONT-SENIOR

console.log(1 + '1' - 1);


теория
- Замыкания
- Создать утечку памяти
Практика
- Счётчик через замыкания
Функция Counter которая возвращает методы set - записывает значение и get - возвращает результат
*/

const counter = () => {
    let closureVar = 0;

    return {
        set: (n) => (closureVar += n),
        get: () => closureVar
    };
};

/*
*  Задача: В коде представлена функция, которая использует замыкание и setTimeout.
* Исправьте ошибки так, чтобы код работал корректно и выводил числа от 1 до 5 с интервалом в 1000 миллисекунд.
* */

for (var i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log(i);
    }, 1000);
}

/*
* Найти проблемы
* */
function duplicate(array) {
    for (let i = 0; i < array.length; i++) {
        array.push(array[i]);
    }
    return array;
}

const arr = [1, 2, 3];
const newArr = duplicate(arr);
console.log(newArr);


/*
Практика
- Функция обёртка для  мемоизации
При вызове функции с одинаковыми аргументами выдавать один результат не вызывая исходную функцию повторно
*/

function memoize(func) {
    let cache = {};

    return (...args) => {
        const key = JSON.stringify(args);

        return cache[key] || (cache[key] = func(...args));
    };
}

/*
Практика
- Функция разбивающая текст на минимальное количество фрагментов не больше определённой длины не разделяя слова
*/

const initialStr =
    "Lorem ipsum dolor sit amet consectetur adipiscing elit Lorem ipsum dolor sit amet consectetur adipiscing elit Lorem ipsum dolor sit amet consectetur adipiscing elit";
// Результат:
// [
//   'Lorem ipsum dolor sit amet',
//   'consectetur adipiscing elit'
// ]

function divideTextIntoChunks(text) {
    const CHUNK_SIZE = 30,
        words = text.split(" "),
        chunks = [];

    let currentChunk = "";

    words.forEach((_, i) => {
        const word = words[i];

        if (currentChunk.length + word.length >= CHUNK_SIZE) {
            chunks.push(currentChunk.trim());
            currentChunk = "";
        }
        currentChunk += `${word} `;
    });

    if (currentChunk.length > 0) {
        chunks.push(currentChunk.trim());
    }

    return chunks;
}

/*
    ЗАДАЧА
* Есть массив строк в этих строках лежать адреса json файлов,
* как написать функцию которая скачает и распарсит все эти файлы
*/


/*
*           R E A C T
* */
/*
- Как работает React?

- Что такое виртуальный DOM?

- В чем разница между классовыми и функциональными компонентами?

- Что такое умные/глупые компоненты

- Что такое props drilling? Чем хорошо/плохо?

- Что такое методы жизненного цикла компонента?

- Как выполнить рендеринг элементов массива?
- Зачем нужны ключи в списках при использовании map()?

- Какие хуки вы знаете?
useState -	управления состоянием в функциональных компонентах
useEffect -	выполнения побочных эффектов в функциональных компонентах (например, для получения данных или подписки на события)
useContext -	доступа к значению контекста React в функциональном компоненте
useRef - создания изменяемых ссылок на элементы или значения, которые сохраняются во время рендеринга
useCallback - мемоизации функций для предотвращения ненужных повторных рендеров
useMemo	- запоминания значений с целью повышения производительности за счет кэширования ресурсоемких вычислений
useReducer - управления состоянием с помощью функции reducer, аналогично тому, как это делается в Redux
useLayoutEffect - выполнения побочных эффектов подобно useEffect, с той разницей, что эффект запускается синхронно после всех мутаций DOM

- В чем заключаются особенности использования useEffect? (Побочные эффекты)

- Какие варианты вызова хука useEffect знаешь? в чём различия?

- Как получить доступ к элементу DOM? (React.createRef() или хука useRef())

-  Перечислите правила создания кастомного хука.
        Название хука должно начинаться с use.
        Нельзя вызывать хук условно. Хук надо вызывать тогда, когда это необходимо.
        Хук должен быть чистой функцией.

*/
/*
чем React.memo отличается от React.useMemo
*/


/*
* 2. React: "Ошибка в работе useEffect"
*    Задача: В компоненте React используется useEffect для загрузки данных из API и установки их в состояние.
*    Однако, есть ошибка, из-за которой запрос к API выполняется бесконечно. Исправьте код, чтобы избежать этой проблемы.
* */

const MyComponent = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        fetchData();
    }, [data]);

    const fetchData = async () => {
        const response = await fetch('https://api.example.com/data');
        const result = await response.json();
        setData(result);
    };

    return (
        <div>
            {/* Render data */}
        </div>
    );
};
/*
*       R E D U X
*
*   - Что такое менеджер состояний и с какими из них вы работали?
*   - Какой паттерн реализует Redux? (Flux)
*   - Что такое reducer в Redux и какие параметры он принимает?
*   - Что такое Action и как можно изменить состояние в Redux?
*   - В каких случаях можно использовать локальное состояние, а когда следует использовать глобальное состояние?
* */


/*
*           О Б Щ И Е
*
* - двустороннее взаимодействие с сервером
* - Что такое линтеры?
* */


/*
*   S S R
*   - Что такое серверный рендеринг (Server-Side Rendering)?
*   - Перечислите преимущества серверного рендеринга.
* */


/*
*   D O C K E R
*
* -
*
* */
